.. _example:

Code Examples
=============

The following section present simple codes in C++ and Python to use CPC device.

Communication
-------------

This first example shows how to list all CPC connected to a computer and how to open and close USB communication. Device information is also recovered in this example.

C++ program
~~~~~~~~~~~

.. code-block:: c++

	#include <iostream>
	using namespace std;

	#include "CPC.h"

	int main(int argc, const char* argv[]) {
	    short iDev = 0;
	    short ret;
	    char* devicesList[10];
	    short numberDevices;
	    char* pch;
	    char* next_pch = NULL;
	    char version[64];
	    char versionParam[3][32];
	    char systemName[6];

	    memset(version, ' ', 64);
	    memset(systemName, '\0', 6);

	    /*	listDevices function	*/
	    // List Aurea Technology devices: MANDATORY BEFORE EACH OTHER ACTION ON THE SYSTEM
	    if (CPC_listDevices(devicesList, &numberDevices) == 0) {
	        if (numberDevices == 0) {
	            cout << endl << "	Please connect device !" << endl << endl;
	            do {
	                delay(500);
	                CPC_listDevices(devicesList, &numberDevices);
	            } while (numberDevices == 0);
	        }
	    }

	    if (numberDevices > 1) {				// If more 1 device is present, list devices available
	        cout << endl << "Device(s) available:" << endl << endl;

	        for (int i = 0; i < numberDevices; i++) {
	            printf(" -%u: %s\n", i, devicesList[i]);
	        }
	        cout << endl << "Select device to drive: ";
	        cin >> iDev;

	        if (CPC_openDevice(iDev) != 0) {			// Open and initialize device: MANDATORY BEFORE EACH OTHER ACTION ON THE SYSTEM
	            cout << "Failed to open CPC" << endl;
	        }
	    }
	    else {							// Open by default only the device connected
	        iDev = 0;
	        if (CPC_openDevice(iDev) != 0) {			// Open and initialize device: MANDATORY BEFORE EACH OTHER ACTION ON THE SYSTEM
	            cout << "Failed to open CPC" << endl;
	        }
	    }

	    // System version recovery
	    if (CPC_getSystemVersion(iDev, version) == 0) {			// Recovery of the system version
	        cout << endl << "  * System version:" << endl << endl;
	    }
	    else {
	        cout << endl << " -> Failed to get system version" << endl << endl;
	    }

	    // Loop to extract CPC parameters
	    int v = 0;
	    pch = secure_strtok(version, ":", &next_pch);
	    while (pch != NULL) {
	        snprintf((char*)&versionParam[v][0], 32, "%s", pch);
	        pch = secure_strtok(NULL, ":", &next_pch);
	        v++;
	    }
	    if (pch != 0) { snprintf((char*)&versionParam[v][0], 32, "%s", pch); }

	    // Show system identity
	    memcpy(systemName, (char*)&versionParam[2][0] + 3, 3);
	    cout << "	AT System       : " << systemName << endl;
	    cout << "	Serial number   : " << versionParam[0] << endl;
	    cout << "	Product number  : " << versionParam[1] << endl;
	    cout << "	Firmware version: " << versionParam[2] << endl;
	    cout << endl;

	    // Wait some time
	    delay(2000);

	    /*	CloseDevice function	*/
	    // Close initial device opened: MANDATORY AFTER EACH END OF SYSTEM COMMUNICATION.
	    if (CPC_closeDevice(iDev) == 0) cout << "   -> Communication closed" << endl;
	    else cout << "  -> Failed to close communication" << endl;

	    return 0;
	}

Python program
~~~~~~~~~~~~~~

.. code-block:: python

	from ctypes import *
	import time

	# Import CPC wrapper file  
	import CPC_wrapper as CPC 

	# Application main
	def main():
		key = ''
		iDev = c_short(0)
		nDev = c_short()
		devList = []

		# Scan and open selected device
		devList,nDev=CPC.listDevices()
		if nDev==0:   # if no device detected, wait
			print ("No device connected, waiting...")
			while nDev==0:
			    devList,nDev=CPC.listDevices()
			    time.sleep(1)
		elif nDev>1:  # if more 1 device detected, select target
			print("Found " + str(nDev) + " device(s) :")
			for i in range(nDev):
			    print (" -"+str(i)+": " + devList[i])
			iDev=int(input("Select device to open (0 to n):")) 

		# Open device
		if CPC.openDevice(iDev)<0:
			input(" -> Failed to open device, press enter to quit !")
			return 0	
		print("Device correctly opened")

		# Recover system version
		ret,version = CPC.getSystemVersion(iDev)
		if ret<0: print(" -> failed\n")
		else:print("System version = {} \n".format(version))

		# Wait some time
		time.sleep(2)

		# Close device communication
		CPC.closeDevice(iDev)

	# Python main entry point
	if __name__ == "__main__":
		main() 


Recover Data
------------

The next example shows how to use CPC to recover clock and photon count. You can place the function CPC_getCLKCountData in a loop in order to get multiple data.

C++ program
~~~~~~~~~~~

.. code-block:: c++

	#include <iostream>
	using namespace std;

	#include "CPC.h"

	int main(int argc, const char* argv[]) {
	    short iDev = 0;
	    short ret;
	    char* devicesList[10];
	    short numberDevices;
	    unsigned long CLK = 0, Count = 0;

	    /*	listDevices function	*/
	    // List Aurea Technology devices: MANDATORY BEFORE EACH OTHER ACTION ON THE SYSTEM
	    if (CPC_listDevices(devicesList, &numberDevices) == 0) {
	        if (numberDevices == 0) {
	            cout << endl << "	Please connect device !" << endl << endl;
	            do {
	                delay(500);
	                CPC_listDevices(devicesList, &numberDevices);
	            } while (numberDevices == 0);
	        }
	    }

	    if (numberDevices > 1) {				// If more 1 device is present, list devices available
	        cout << endl << "Device(s) available:" << endl << endl;

	        for (int i = 0; i < numberDevices; i++) {
	            printf(" -%u: %s\n", i, devicesList[i]);
	        }
	        cout << endl << "Select device to drive: ";
	        cin >> iDev;

	        if (CPC_openDevice(iDev) != 0) {			// Open and initialize device: MANDATORY BEFORE EACH OTHER ACTION ON THE SYSTEM
	            cout << "Failed to open CPC" << endl;
	        }
	    }
	    else {							// Open by default only the device connected
	        iDev = 0;
	        if (CPC_openDevice(iDev) != 0) {			// Open and initialize device: MANDATORY BEFORE EACH OTHER ACTION ON THE SYSTEM
	            cout << "Failed to open CPC" << endl;
	        }
	    }

	    // Recover Clock and Photons count
	    if (CPC_getCLKCountData(iDev, &CLK, &Count) != 0)
	        cout << " -> ! data not match !" << endl;
	    else
	        printf("\r -> Clock: %7lu	Hz	Counts: %7lu", CLK, Count);

	    // Wait some time
	    delay(2000);

	    /*	CloseDevice function	*/
	    // Close initial device opened: MANDATORY AFTER EACH END OF SYSTEM COMMUNICATION.
	    if (CPC_closeDevice(iDev) == 0) cout << "   -> Communication closed" << endl;
	    else cout << "  -> Failed to close communication" << endl;

	    return 0;
	}

Python program
~~~~~~~~~~~~~~

.. code-block:: python

	from ctypes import *
	import time

	# Import CPC wrapper file  
	import CPC_wrapper as CPC 

	# Application main
	def main():
		key = ''
		iDev = c_short(0)
		nDev = c_short()
		devList = []

		# Scan and open selected device
		devList,nDev=CPC.listDevices()
		if nDev==0:   # if no device detected, wait
			print ("No device connected, waiting...")
			while nDev==0:
		    	devList,nDev=CPC.listDevices()
		    	time.sleep(1)
		elif nDev>1:  # if more 1 device detected, select target
			print("Found " + str(nDev) + " device(s) :")
			for i in range(nDev):
		    	print (" -"+str(i)+": " + devList[i])
			iDev=int(input("Select device to open (0 to n):")) 

		# Open device
		if CPC.openDevice(iDev)<0:
			input(" -> Failed to open device, press enter to quit !")
			return 0	
		print("Device correctly opened")

		# Recover Clock and Photons Count
		time.sleep(2)
		ret,clk,det=CPC.getClockDetData(iDev)
		if ret<0: print(" -> failed\n")
		else: print(" Clock     = {} Hz \n Detection = {} cnt\s \n".format(clk.value,det.value))

		# Wait some time
		time.sleep(2)

		# Close device communication
		CPC.closeDevice(iDev)

	# Python main entry point
	if __name__ == "__main__":
		main() 

.. note::

	All function information is available in section :ref:`All Functions`.
