.. C++ Sphinx Doxygen Breathe documentation master file, created by
   sphinx-quickstart on Fri Oct 15 12:11:42 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CPC API guide !
==============================

The Compact Photons Counter (CPC) can be controlled and used with a computer through the USB connection. To do that we provide an API (Application Programming Interface) based on a library developed in C/C++ language for all operating systems (Windows, MacOS, Linux).

Through it, you can develop your own CPC control interface.
In order to help you, we provide the library files, and some examples in C++ and Python for all operating systems.

.. toctree::
   :maxdepth: 3
   
   installation/installation
   custom_application/custom_application
   example/example
   api/index
   wrapper/wrapper
   revision/revision