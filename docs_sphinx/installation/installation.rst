.. _installation:

Software Installation Guide
===========================

The following section describes how to install the CPC software on Windows, MacOS and Linux operating systems.

Windows
-------

Operating Systems Requirements
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

	- Windows 7 or higher

	- Application and examples are working on 32bit and 64bit systems.
	

Installation Step
^^^^^^^^^^^^^^^^^

	#. Run the setup file locate in provided directory.

	#. Connect CPC device to your computer with the USB cable.

	#. Start Aurea-CPC application or start Aurea-Launcher and then click on your device to use the software. 


MacOS
-----

	- Aurea-Launcher Installation :

		#. Double click on Aurea-Launcher.dmg file.

		#. Drag Aurea-Launcher in the Applications folder.

	- Aurea-CPC Installation :

		#. Double click on Aurea-CPC.dmg file.

		#. Drag Aurea-CPC in the Applications folder

		#. Connect CPC device to your computer with the USB cable.

		#. Launch Aurea-CPC or Aurea-Launcher application by clicking on it.

Linux
-----
	
	- Aurea-Launcher Installation :

		#. Unzip Aurea-Launcher-package.zip

		#. Go to Aurea-Launcher-package/Aurea-Launcher and double-click on Aurea-Launcher-Installer.

		#. Follow the installer instructions and make sure to install all Aurea Technology software in the same directory.

	- Aurea-CPC Installation :

		#. Unzip Aurea-CPC-package.zip

		#. Go to Aurea-CPC-package/Aurea-CPC and double-click on Aurea-CPC-Installer.

		#. Follow the installer instructions and make sure to install all Aurea Technology software in the same directory.

		#. Connect CPC device to your computer with the USB cable.

		#. Launch Aurea-CPC or Aurea-Launcher application by executing the following command in the installation directory.

	.. code-block:: console

	    ./Aurea-Launcher.sh

	.. code-block:: console

	    ./Aurea-CPC.sh